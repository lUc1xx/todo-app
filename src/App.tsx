import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import TodoForm from "./TodoForm";
import {TodoList} from "./TodoList";
import {TodoItemsContextProvider} from "./TodoContext";

function App() {
    return (
        <TodoItemsContextProvider>
            <Container maxWidth="sm">
                <header>
                    <Typography variant="h2" component="h1">
                        Todo List
                    </Typography>
                </header>
                <main>
                    <TodoForm/>
                    <TodoList/>
                </main>
            </Container>
        </TodoItemsContextProvider>
    );
}

export default App;
