export enum ActionType {
    loadState = 'loadState',
    add = 'add',
    delete = 'delete',
    toggleDone = 'toggleDone',
    reorder = 'reorder',
    edit = 'edit',
}

export interface ITodoItem {
    id: string;
    title: string;
    details?: string;
    done: boolean;
}

export interface ITodoItemsState {
    todoItems: ITodoItem[];
}

export type TodoItemAdd = { title: string, details?: string };

export type Action = ActionLoadState | ActionReorder | ActionAdd | ActionDelete | ActionToggleDone | ActionEdit;


interface ActionLoadState {
    type: ActionType.loadState,
    payload: ITodoItemsState
}

interface ActionReorder {
    type: ActionType.reorder,
    payload: ITodoItemsState
}

interface ActionAdd {
    type: ActionType.add;
    payload: TodoItemAdd
}

interface ActionDelete {
    type: ActionType.delete;
    payload: { id: string };
}

interface ActionToggleDone {
    type: ActionType.toggleDone;
    payload: { id: string };
}

interface ActionEdit {
    type: ActionType.edit;
    payload: { id: string, title: string, details?: string }
}