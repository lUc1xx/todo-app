import {makeStyles} from '@material-ui/core/styles';
import {motion} from 'framer-motion';
import {DragDropContext, Draggable, Droppable} from 'react-beautiful-dnd';
import TodoItem from "./TodoItem";
import {useTodoItems} from './TodoContext';
import {ActionType, ITodoItem} from "./TodoTypes";

const spring = {
    type: 'spring',
    damping: 25,
    stiffness: 120,
    duration: 0.25,
};

const useTodoListStyles = makeStyles({
    root: {
        listStyle: 'none',
        padding: 0,
    },
});

export const TodoList = function () {
    const {todoItems, dispatch} = useTodoItems();
    const classes = useTodoListStyles();

    const onDragEnd = (result: any): void => {
        if (!result.destination) return;
        if (result.destination.index === result.source.index) return;

        const reorderTodoItems = reorderItems(
            todoItems,
            result.source.index,
            result.destination.index
        );

        dispatch({type: ActionType.reorder, payload: {todoItems: reorderTodoItems}})
    };
    return (
        <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="items">
                {provided => (
                    <ul className={classes.root} {...provided.droppableProps} ref={provided.innerRef}>
                        {todoItems.map((item: ITodoItem, index: number) => (
                            <Draggable key={item.id} draggableId={item.id} index={index}>
                                {provided => (
                                    <div ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                                        <motion.li key={item.id} transition={spring} layout={true}>
                                            <TodoItem item={item}/>
                                        </motion.li>
                                    </div>
                                )}
                            </Draggable>

                        ))}
                    </ul>
                )}
            </Droppable>
        </DragDropContext>
    );
};

const reorderItems = (list: ITodoItem[], startIndex: number, endIndex: number): ITodoItem[] => {
    const [selected] = list.splice(startIndex, 1);
    list.splice(endIndex, 0, selected);

    return list;
};