import {
    createContext,
    ReactNode,
    useContext,
    useEffect,
    useReducer,
} from 'react';
import {Action, ITodoItemsState, ActionType} from "./TodoTypes";

const TodoItemsContext = createContext<(ITodoItemsState & { dispatch: (action: Action) => void }) | null>(null);

const defaultState = {todoItems: []};
const localStorageKey = 'todoListState';

export const TodoItemsContextProvider = ({children}:{children?: ReactNode
}) => {
    const [state, dispatch] = useReducer(todoItemsReducer, defaultState);

    useEffect(() => {
        const savedState = localStorage.getItem(localStorageKey);

        if (savedState) {
            try {
                dispatch({type: ActionType.loadState, payload: JSON.parse(savedState)});
            } catch {}
        }
    }, []);

    useEffect(() => {
        localStorage.setItem(localStorageKey, JSON.stringify(state));
    }, [state]);

    return (
        <TodoItemsContext.Provider value={{...state, dispatch}}>
            {children}
        </TodoItemsContext.Provider>
    );
};

export const useTodoItems = () => {
    const todoItemsContext = useContext(TodoItemsContext);

    if (!todoItemsContext) {
        throw new Error(
            'useTodoItems hook should only be used inside TodoItemsContextProvider',
        );
    }

    return todoItemsContext;
};

function todoItemsReducer(state: ITodoItemsState, action: Action) {
    switch (action.type) {
        case ActionType.loadState: {
            return action.payload;
        }
        case ActionType.add:
            return {
                ...state,
                todoItems: [
                    {id: generateId(), done: false, ...action.payload},
                    ...state.todoItems,
                ],
            };
        case ActionType.delete:
            return {
                ...state,
                todoItems: state.todoItems.filter(
                    ({id}) => id !== action.payload.id,
                ),
            };
        case ActionType.toggleDone:
            const itemIndex = state.todoItems.findIndex(
                ({id}) => id === action.payload.id,
            );
            const item = state.todoItems[itemIndex];

            return {
                ...state,
                todoItems: [
                    ...state.todoItems.slice(0, itemIndex),
                    {...item, done: !item.done},
                    ...state.todoItems.slice(itemIndex + 1),
                ],
            };
        case ActionType.reorder: {
            return action.payload
        }

        case ActionType.edit: {
            const list = state.todoItems.map(item =>
                item.id === action.payload.id ?
                    {
                        ...item,
                        title: action.payload.title,
                        details: action.payload.details
                    } : item
            );
            return {...state, todoItems: list}
        }

        default:
            throw new Error();
    }
}

function generateId() {
    return `${Date.now().toString(36)}-${Math.floor(
        Math.random() * 1e16,
    ).toString(36)}`;
}
